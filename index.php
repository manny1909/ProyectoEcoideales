
<?php $palabra="";
      $pid="";    
      session_start();
      require_once "Logica/Proveedor.php";
      require_once "Logica/Administrador.php";
      require_once "Logica/Cliente.php";
      require_once "Logica/Producto.php";
      $pid = "";
      if(isset($_GET["pid"])){
          $pid = base64_decode($_GET["pid"]);
      }else{
          $_SESSION["id"]="";
          $_SESSION["rol"]="";
      }
      if(isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])){
          $_SESSION["id"]="";
      }

?>
<html>

<head>
	<title>Ecoideales joyeria <?php echo $palabra?></title>
	<link rel="icon" type="image/png" href="img/Logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
	
</head>
<body style="background-color: black">




<?php 
	$paginasSinSesion = array(
        "Presentacion/autenticar.php",
	    "presentacion/cliente/registrarCliente.php",
	    "presentacion/cliente/activarCliente.php",
	    "Presentacion/Catalogo.php"
	);	
	if(in_array($pid, $paginasSinSesion)){
	    include "Presentacion/encabezado.php";
	    include $pid;
	}else if($_SESSION["id"]!="") {
	    if($_SESSION["rol"] == "Administrador"){
	        include "Presentacion/menuAdministrador.php";
	    }else if($_SESSION["rol"] == "Cliente"){
	        include "Presentacion/menuCliente.php";
	    }
	    elseif ($_SESSION["rol"]=="Proveedor"){
	        include "Presentacion/menuProveedor.php";
	    }
	    include $pid;
	}else{
	    include "Presentacion/encabezado.php";
	    include "Presentacion/inicio.php";
	    
	}
	?>	



</body>
</html>