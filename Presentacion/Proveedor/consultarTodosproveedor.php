<?php
$Proveedor = new Proveedor();
$Proveedor= $Proveedor -> consultarTodosproveedor();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Producto del proveedor</h4>
				</div>
				<div class="text-right"><?php echo count($productos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
						</tr>
						<?php 
						$i=1;
						foreach($productos as $productoActuales){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $productoActuales -> getNombre() . "</td>";
						    echo "<td>" . $productoActuales -> getCantidad() . "</td>";
						    echo "<td>" . $productoActuales -> getPrecio() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					<div class="text-center">
        				<nav>
        					<ul class="pagination">
        						<li class="page-item disabled"><span class="page-link"> &lt;&lt; </span>
        						</li>
        						<li class="page-item"><a class="page-link" href="index.php?pid=<?php echo base64_encode("presentacion/Proveedor/consultarTodosproveedor.php") ?>&pagina=1">1</a></li>
        						<li class="page-item active" aria-current="page"><span
        							class="page-link"> 2 <span class="sr-only">(current)</span>
        						</span></li>
        						<li class="page-item"><a class="page-link" href="#">3</a></li>
        						<li class="page-item"><a class="page-link" href="#"> &gt;&gt; </a></li>
        					</ul>
        				</nav>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>