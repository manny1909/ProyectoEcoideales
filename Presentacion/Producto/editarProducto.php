<?php
if(isset($_POST["editar"])){ 
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "imagenes/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $producto = new Producto($_GET["idProducto"]);
        $producto -> consultar(); 
        if($producto -> getImagen() != ""){
            unlink($producto -> getImagen());
        }
        $producto = new Producto($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"], $rutaRemota, $_POST["descuento"],$_GET["idProveedor"]);
        $producto -> editar();        
    }else{
        $producto = new Producto($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"],"",$_POST["descuento"],$_GET["idProveedor"]);
        $producto -> editar();
    }
}else{
    $producto = new Producto($_GET["idProducto"]);
    $producto -> consultar();    
}
?>
<div class="container mt-3"> 
			<?php echo "hola". $producto->getProductoDAO()->editar();?>
	
	<div class="row">
	
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Editar Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["editar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/editarProducto.php") ?>&idProducto=<?php echo $_GET["idProducto"]?> &idProveedor=<?php echo $_GET["idProveedor"]?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $producto -> getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $producto -> getCantidad() ?>" required>
						</div>
						<div class="form-group">
							<label>Precio</label> 
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $producto -> getPrecio() ?>" required>
						</div>
						<div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						<div class="form-group">
							<label>Descuento</label> 
							<input type="number" name="descuento" class="form-control" min="0" value="<?php echo $producto -> getDescuento() ?>" required>
						</div>
						<button type="submit" name="editar" class="btn btn-info">Editar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>