<?php
$filtro = $_GET["filtro"];
$producto = new Producto();
$productos = $producto -> consultarFiltro($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Producto</h4>
				</div>
				<div class="text-right"><?php echo count($productos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th>Descuento</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($productos as $productoActual){
// 						    $posiciones = array();
// 						    for($i=0; $i<strlen($productoActual -> getNombre())-strlen($filtro)+1; $i++){
// 						        if(strtolower(substr($productoActual -> getNombre(), $i, strlen($filtro))) == strtolower($filtro)){
// 						            array_push($posiciones, $i);
// 						        }
// 						    }
						    $pos = stripos($productoActual -> getNombre(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td>" . $productoActual -> getNombre() . "</td>";						        
						    }else{						        
						        echo "<td>" . substr($productoActual -> getNombre(), 0, $pos) . "<mark>" . substr($productoActual -> getNombre(), $pos, strlen($filtro)) . "</mark>" . substr($productoActual -> getNombre(), $pos+strlen($filtro)) . "</td>";
						    }
						    echo "<td>" . $productoActual -> getCantidad() . "</td>";
						    echo "<td>" . $productoActual -> getPrecio() . "</td>";
						    echo "<td>" . $productoActual -> getDescuento() . "%</td>";						    
						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/producto/editarProducto.php") . "&idProducto=" . $productoActual -> getIdProducto(). "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-edit'></span></a></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>