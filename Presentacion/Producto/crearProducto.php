<?php
$nombre="";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$cantidad = "";
if(isset($_POST["cantidad"])){
    $cantidad = $_POST["cantidad"];
}
$precio = "";
if(isset($_POST["precio"])){
    $precio = $_POST["precio"];
}    
$descuento = "";
if(isset($_POST["descuento"])){
    $descuento = $_POST["descuento"];
}
$idProveedor="";
if (isset($_POST["idProveedor"])) {
    $idProveedor=$_POST["idProveedor"];
}elseif ($_SESSION["rol"]=="Proveedor"){
    $idProveedor = $_SESSION["id"];
}
$imagen="";
if(isset($_POST["imagen"])){
    $imagen=$_POST["imagen"];
}

if(isset($_POST["crear"])){
    $producto = new Producto("", $nombre, $cantidad, $precio,"", $descuento,$idProveedor);
    $producto -> insertar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>" method="post">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $cantidad ?>" required>
						</div>
						<div class="form-group">
							<label>Precio</label> 
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $precio ?>" required>
						</div>
						<div class="form-group">
							<label>Descuento</label> 
							<input type="number" name="descuento" class="form-control" min="0" value="<?php echo $descuento ?>" required>
						</div>
						<?php 
						require_once "Persistencia/Conexion.php";						
						if($_SESSION["rol"]=="Administrador"){  $cont; $conexion=new Conexion();
						$conexion->abrir();						
						$conexion->ejecutar("select *from proveedor");
						 $aux="";
						 for ($i = 0; $i < $conexion->numFilas(); $i++) {
						     $resultado=$conexion->extraer();
						     $value=$resultado[0];
						     $nProv=$resultado[2];
						     $aux.='<option value="'.$value.'">'.$nProv.'</option>';
						 }
						$conexion->cerrar();
						    echo ' <select class="custom-select" id="inputGroupSelect01" name="idProveedor" >
                                    <option selected>ID Proveedor.</option>'.
                                    $aux.' </select>';
						}
						?> 
						<button type="submit" name="crear" class="btn btn-info mt-3">crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>