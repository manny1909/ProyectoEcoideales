<?php
$proveedor = new Proveedor($_SESSION["id"]);
$proveedor -> consultar();
$items = 1;
if($proveedor -> getnombre() != ""){
    $items++;
}
if($proveedor -> getapellido() != ""){
    $items++;
}
if($proveedor -> getfoto() != ""){
    $items++;
}
$porcentaje = $items/4 * 100;
?>
<nav class="navbar navbar-expand-md navbar-light " style="background-color: #E8EAE8">
	<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionProveedor.php") ?>"><i class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false">Producto</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoTodos.php") ?>">Consultar</a>
				</div></li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false"><span class="badge badge-danger"><?php echo $porcentaje . "%"?></span>
				Proveedor: <?php echo ($proveedor -> getnombre()!=""?$proveedor -> getnombre():$proveedor -> getcorreo()) ?> <?php echo $proveedor -> getapellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="#">Editar Perfil</a> 
					<a class="dropdown-item" href="#">Editar Foto</a>
					<a class="dropdown-item" href="#">Cambiar Clave</a>						
				</div></li>
			<li class="nav-item active"><a class="nav-link" href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>