<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("","", "", "", $correo, $clave, "","","","","","");
$cliente = new Cliente("","", "", "","", $correo, $clave,"","","","","","","","");
$proveedor = new Proveedor("","","","","",$correo,$clave,"","","","","",""); 
if($administrador -> autenticar()){ 
    $_SESSION["id"] = $administrador -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
}else if($cliente -> autenticar()){ 
    if($cliente -> getEstado() == -1){ 
        header("Location: index.php?error=2");
    }else if($cliente -> getEstado() == 0){ 
        header("Location: index.php?error=3");
    }else{ 
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"));
    }  
} 
else if ($proveedor->autenticar()){ 
    if($proveedor -> getEstado() == -1){
        header("Location: index.php?error=2");
    }else if($proveedor -> getEstado() == 0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"] = $proveedor -> getIdProveedor();
        $_SESSION["rol"] = "Proveedor";
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionProveedor.php"));
    }
}
else{
    header("Location: index.php?error=1");
}
?>
