


<div class="card text-center "style="background-color:black;">
  	
  	
  	<div class="card-body">
                <div class="container">
                	<div class="row">
                
                
                <div class=" col-lg-9 col-md-12">
                
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >
                      <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                      </ol>
                      <div class="carousel-inner"align="center">
                        <div class="carousel-item active">
                          <img  src=img/reciclaje.png height="700px" width="600px"  alt="First slide">
                        </div>
                        <div class="carousel-item">
                          <img  src=img/plumas.jpg height="700px" width="600px" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                          <img  src=img/parejaGatos.jpg height="700px" width="600px" alt="Third slide">
                        </div>
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Anterior</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Siguiente</span>
                      </a>
                  </div>
                
             </div>
               <div class="col-lg-3 col-md-6 mt-1 align-self-center" >
               <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Autenticacion</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("Presentacion/autenticar.php") ?>" method="post">
        				
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="ingresar" type="submit" class="form-control btn btn-info">
    					</div>
    					
                          
    					<?php 
    					if(isset($_GET["error"]) && $_GET["error"]==1){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
    					}else if(isset($_GET["error"]) && $_GET["error"]==2){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";    					   
    					}else if(isset($_GET["error"]) && $_GET["error"]==3){
    					    echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
    					}
    					?>
        			</form>
        			<p>Eres nuevo? <a href="index.php?pid=<?php echo base64_encode("presentacion/cliente/registrarCliente.php")?>">Registrate</a></p>   			
            	</div>
            </div>
               </div> 
                
                </div>                 
               </div>
               <?php include "Presentacion/Catalogo.php";?> 
    </div> 
          
  <div class="card-footer text-muted" style="background: #CECFCE">
    2 days ago
  </div>
</div>

