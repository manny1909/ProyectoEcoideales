<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ProductoDAO.php";
class Producto{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $imagen;
    private $descuento;
    private $idProveedor;
    private $conexion;
    private $productoDAO;
    
    
    public function getIdProveedor()
    {
        return $this->idProveedor;
    }

    public function getIdProducto(){
        return $this -> idProducto;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
        
    }
    
    
    public function getPrecio(){
        return $this -> precio;
    }
    public function getDescuento()
    {
        return $this->descuento;
    }
        
    public function getImagen(){
        return $this -> imagen;
    }
    public function getConexion()
    {
        return $this->conexion;
    }
    
    public function getProductoDAO()
    {
        return $this->productoDAO;
    }
    
    public function Producto($idProducto = "", $nombre = "", $cantidad = "", $precio = "",  $imagen = "",$descuento="", $idProveedor=""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> imagen = $imagen;
        $this -> descuento =$descuento;
        $this -> idProveedor =$idProveedor;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($this -> idProducto, $this -> nombre, $this -> cantidad, $this -> precio, $this -> imagen, $this->descuento, $this->idProveedor);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> cantidad = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> imagen = $resultado[3];
        $this -> descuento = $resultado[4];        
        $this -> idProveedor = $resultado[5];
        
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> insertar());        
        $this -> conexion -> cerrar();
        return $this->productoDAO->insertar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5],$resultado[6]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5],$resultado[6]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> editar());
        $this -> conexion -> cerrar();
    }
  
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarFiltro($filtro));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3],"", $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    
}

?>