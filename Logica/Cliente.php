<?php
require_once "Logica/Persona.php";
require_once "persistencia/Conexion.php";
require_once "persistencia/ClienteDAO.php";
class Cliente extends Persona{
    private $idCliente;    
    private $estado; 
    private $tipo;
    private $descuento;
    private $conexion;
    private $clienteDAO;

    
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    
    public function getEstado()
    {
        return $this->estado;
    }

    
    public function getConexion()
    {
        return $this->conexion;
    }

    public function getClienteDAO()
    {
        return $this->clienteDAO;
    }

    public function Cliente($idCliente = "",$estado = "", $nombre = "", $apellido = "",$docId="", $correo = "", $clave = "", $foto = "",$pais="",$departamento="",$ciudad="",$direccion="",$telefono="", $tipo="", $descuento=""){
        $this -> idCliente = $idCliente;
        parent::__construct( $nombre , $apellido ,$docId, $correo , $clave , $foto,$pais,$departamento,$ciudad,$direccion,$telefono);
        $this -> estado = $estado;
        $this -> tipo = $tipo;
        $this -> descuento = $descuento;
        $this -> conexion = new Conexion();
        $this -> clienteDAO = new ClienteDAO($this -> idCliente,$this -> estado, $this -> nombre, $this -> apellido, $this->docId, $this -> correo, $this -> clave, $this -> foto, $this->pais,$this->departamento, $this->ciudad, $this->direccion, $this->telefono );
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarTodos());
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cliente($resultado[0], $resultado[1], $resultado[2], $resultado[3],"", $resultado[4]);
            array_push($clientes, $c);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }    
   
    public function existeCorreo(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> clienteDAO -> existeCorreo());        
        $this -> conexion -> cerrar();        
        return $this -> conexion -> numFilas();
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $codigoActivacion = rand(1000,9999);
        $this -> conexion -> ejecutar($this -> clienteDAO -> registrar($codigoActivacion));
        $this -> conexion -> cerrar();       
        $url = "http://virtualud.itiud.org/index.php?pid=" . 
                    base64_encode("presentacion/cliente/activarCliente.php") . "&correo=" .
                    $this -> correo . "&codigoActivacion=" .
                    base64_encode($codigoActivacion);
        echo $url;
        $asunto = "[Tienda Virtual] Activación de Nuevo Usuario";
        $mensaje = "Active su cuenta con la URL " . $url;
        $encabezado = array(
            "From" => "contacto@itiud.org",
            "Cc" => "haflorezf@udistrital.edu.co",
            "Bcc" => "hectorarturo@yahoo.com"
        );
        mail($this -> correo, $asunto, $mensaje, $encabezado);        
    }
    
    public function activarCliente($codigoActivacion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> verificarCodigoActivacion($codigoActivacion));                
        if ($this -> conexion -> numFilas() == 1){
            $this -> conexion -> ejecutar($this -> clienteDAO -> activar());
            $this -> conexion -> cerrar();
            return true;
        }else {
            $this -> conexion -> cerrar();
            return false;
        }        
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idCliente = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }
 
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> cambiarEstado());
        $this -> conexion -> cerrar();        
    }
    
    
}

?>