<?php
require_once "Logica/Persona.php";
require_once "persistencia/Conexion.php";
require_once "persistencia/ProveedorDAO.php";
class Proveedor extends Persona{
    private $idProveedor;    
    private $estado;      
    private $conexion;
    private $proveedorDAO;

    
    public function getIdProveedor()
    {
        return $this->idProveedor;
    }

    
    public function getEstado()
    {
        return $this->estado;
    }

    
    public function getConexion()
    {
        return $this->conexion;
    }

    public function getProveedorDAO()
    {
        return $this->proveedorDAO;
    }

    public function Proveedor($idProveedor = "",$estado = "", $nombre = "", $apellido = "",$docId="", $correo = "", $clave = "", $foto = "", $pais="",$departamento="",$ciudad="",$direccion="",$telefono=""){
        $this -> idProveedor = $idProveedor;
        parent::__construct( $nombre , $apellido ,$docId, $correo , $clave ,$foto, $pais,$departamento,$ciudad,$direccion,$telefono );
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> proveedorDAO = new ProveedorDAO($this -> idProveedor,$this -> estado, $this -> nombre, $this -> apellido, $this->docId ,$this-> correo, $this -> clave, $this -> foto, $this->pais,$this->departamento,$this->ciudad,$this->direccion,$this->telefono);
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarTodos());
        $proveedores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Proveedor($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", "", $resultado[4]);
            array_push($proveedores, $p);
        }
        $this -> conexion -> cerrar();
        return $proveedores;
    }    
   
    public function existeCorreo(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> proveedorDAO -> existeCorreo());        
        $this -> conexion -> cerrar();        
        return $this -> conexion -> numFilas();
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $codigoActivacion = rand(1000,9999);
        $this -> conexion -> ejecutar($this -> proveedorDAO -> registrar($codigoActivacion));
        $this -> conexion -> cerrar();       
        $url = "http://virtualud.itiud.org/index.php?pid=" . 
                    base64_encode("presentacion/cliente/activarCliente.php") . "&correo=" .
                    $this -> correo . "&codigoActivacion=" .
                    base64_encode($codigoActivacion);
        echo $url;
        $asunto = "[Tienda Virtual] Activación de Nuevo Usuario";
        $mensaje = "Active su cuenta con la URL " . $url;
        $encabezado = array(
            "From" => "contacto@itiud.org",
            "Cc" => "haflorezf@udistrital.edu.co",
            "Bcc" => "hectorarturo@yahoo.com"
        );
        mail($this -> correo, $asunto, $mensaje, $encabezado);        
    }
    
    public function activarProveedor($codigoActivacion){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> verificarCodigoActivacion($codigoActivacion));                
        if ($this -> conexion -> numFilas() == 1){
            $this -> conexion -> ejecutar($this -> proveedorDAO -> activar());
            $this -> conexion -> cerrar();
            return true;
        }else {
            $this -> conexion -> cerrar();
            return false;
        }        
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idProveedor = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }
 
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> cambiarEstado());
        $this -> conexion -> cerrar();        
    }
    
    
}

?>