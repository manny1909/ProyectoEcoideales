<?php
require_once "Logica/Persona.php";
require_once "Persistencia/Conexion.php";
require_once "Persistencia/AdministradorDAO.php";
class Administrador extends Persona{
    private  $idAdministrador;
    private $conexion;
    private $administradorDAO;

    public function getIdAdministrador()
    {
        return $this->idAdministrador;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    
    public function getAdministradorDAO()
    {
        return $this->administradorDAO;
    }

    public function Administrador($idAdministrador = "", $nombre = "", $apellido = "", $docId="", $correo="" , $clave="" ,  $foto = "",$pais="",$departamento="", $ciudad="",$direccion="",$telefono=""){
        parent::__construct($nombre , $apellido , $docId, $correo, $clave, $foto ,$pais,$departamento, $ciudad,$direccion,$telefono);
        $this -> idAdministrador = $idAdministrador;
        $this -> conexion = new Conexion();
        $this -> administradorDAO = new AdministradorDAO($this -> idAdministrador, $this -> nombre, $this -> apellido,$this -> docId,  $this -> correo, $this -> clave, $this -> foto, $this -> pais, $this ->departamento, $this -> direccion, $this -> telefono);
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO->autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAdministrador = $resultado[0];             
            return true;        
        }else {
            
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultar()); 
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> docId = $resultado[2];        
        $this -> correo = $resultado[3];
        $this -> foto = $resultado[4];
        
    }
    
}

?>
