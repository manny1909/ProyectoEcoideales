<?php 
    class Persona {
        
        protected $nombre;
        protected $apellido;
        protected $docId;
        protected $correo;
        protected $clave;
        protected $pais;
        protected $departamento;
        protected $ciudad;
        protected $direccion;
        protected $telefono;
        protected $foto;
        
        public function getNombre()
        {
            return $this->nombre;
        }
    
        public function getApellido()
        {
            return $this->apellido;
        }
    
        public function getDocId()
        {
            return $this->docId;
        }
    
        public function getCorreo()
        {
            return $this->correo;
        }
    
        public function getClave()
        {
            return $this->clave;
        }
    
        public function getPais()
        {
            return $this->pais;
        }
    
        public function getDepartamento()
        {
            return $this->departamento;
        }
    
        public function getCiudad()
        {
            return $this->ciudad;
        }
    
        public function getDireccion()
        {
            return $this->direccion;
        }
    
        public function getTelefono()
        {
            return $this->telefono;
        }
    
        public function getFoto()
        {
            return $this->foto;
        }
    
        public function Persona($nombre="", $apellido="", $docId="", $correo="", $clave="",$foto="", $pais="", $departamento="", $ciudad="", $direccion="",$telefono="") {
            $this -> nombre =$nombre;
            $this -> apellido = $apellido;
            $this -> docId = $docId;
            $this -> correo = $correo;
            $this -> clave = $clave;
            $this -> foto = $foto;
            $this -> pais = $pais;
            $this -> departamento = $departamento;
            $this -> ciudad = $ciudad;
            $this -> direccion = $direccion;
            $this -> telefono = $telefono;
            
        }
        
        }
        
?>