<?php

require_once "Logica/Persona.php";
class ClienteDAO extends Persona{
    private $idCliente;    
    private $estado;
    private $tipo;
    private $descuento;
    
    public function getTipo()
    {
        return $this->tipo;
    }

    public function getDescuento()
    {
        return $this->descuento;
    }

    public function getIdCliente()
    {
        return $this->idCliente;
    }
    public function getEstado()
    {
        return $this->estado;
    }

    public function ClienteDAO($idCliente = "",$estado = "", $nombre = "", $apellido = "",$docId="", $correo = "", $clave = "", $foto = "",$pais="",$departamento="",$ciudad="",$direccion="",$telefono="", $tipo="", $descuento="" ){
        $this -> idCliente = $idCliente;        
        $this -> estado = $estado;
        $this -> tipo = $tipo;
        $this -> descuento = $descuento;
        parent::__construct( $nombre , $apellido ,$docId, $correo , $clave , $foto,$pais,$departamento,$ciudad,$direccion,$telefono);
        
    }
    
    public function existeCorreo(){
        return "select correo
                from Cliente
                where correo = '" . $this -> correo .  "'";
    }
        
    public function registrar($codigoActivacion){
        return "insert into Cliente (correo, clave, estado)
                values ('" . $this -> correo . "', '" . md5($this -> clave) . "', '-1')";
    }
 
    
    
    public function activar(){
        return "update Cliente 
                set estado = '1'
                where correo = '" . $this -> correo .  "'";
    }
    
    public function autenticar(){
        return "select idCliente, estado
                from Cliente
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto
                from Cliente
                where idcliente = '" . $this -> idCliente .  "'";
    }
    
    public function consultarTodos(){
        return "select idCliente, estado, nombre, apellido, correo
                from Cliente";
    }
    
    public function cambiarEstado(){
        return "update Cliente
                set estado = '" . $this -> estado . "'
                where idCliente = '" . $this -> idCliente .  "'";
    }
    
}

?>