<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $imagen;
    private $descuento;
    private $idProveedor;
       
    public function ProductoDAO($idProducto = "", $nombre = "", $cantidad = "", $precio = "", $imagen = "", $descuento="",$idProveedor=""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;        
        $this -> imagen = $imagen;
        $this -> descuento = $descuento;
        $this -> idProveedor = $idProveedor;
        
        
    }

    public function getIdProducto()
    {
        return $this->idProducto;
    }
    public function getNombre()
    {
        return $this->nombre;
    }
    public function getCantidad()
    {
        return $this->cantidad;
    }
    public function getPrecio()
    {
        return $this->precio;
    }
    public function getImagen()
    {
        return $this->imagen;
    }

    public function getDescuento()
    {
        return $this->descuento;
    }

    public function getIdProveedor()
    {
        return $this->idProveedor;
    }

    public function consultar(){
        return "select Nombre, Cantidad, Precio,Imagen, Descuento,  Proveedor_idProveedor
                from Producto
                where idProducto = '" . $this -> idProducto .  "'";
    }    
    
    public function insertar(){
        return "insert into Producto (Nombre, Cantidad, Precio, Descuento, Proveedor_idProveedor)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio ."','". $this->descuento. "','".$this->idProveedor."')";
    }
    
    public function consultarTodos(){
        return "select idProducto, nombre, cantidad, precio,  imagen, descuento, Proveedor_idProveedor
                from Producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, cantidad, precio, imagen, descuento, Proveedor_idProveedor
                from Producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idProducto)
                from Producto";
    }
 
    public function editar(){
        return "update Producto
                set nombre = '" . $this -> nombre . "', cantidad = '" . $this -> cantidad . "', precio = '" . $this -> precio . "'".", descuento = '".$this->descuento ."'".  (($this -> imagen!="")?", imagen = '" . $this -> imagen . "'":"") . "
                where idProducto = '" . $this -> idProducto .  "'";
    }
  
    public function consultarFiltro($filtro){
        return "select idProducto, nombre, cantidad, precio, descuento
                from Producto
                where nombre like '%" . $filtro . "%' or cantidad like '" . $filtro . "%' or precio like '" . $filtro . "%' or descuento like '" . "%'";
    }
    
}

?>