<?php

require_once "Logica/Persona.php";
class ProveedorDAO extends Persona{
    private $idProveedor;    
    private $estado;

    public function getIdCliente()
    {
        return $this->idProveedor;
    }
    public function getEstado()
    {
        return $this->estado;
    }

    public function ProveedorDAO($idProveedor = "",$estado = "", $nombre = "", $apellido = "",$docId="", $correo = "", $clave = "", $foto = "",$pais="",$departamento="",$ciudad="",$direccion="",$telefono=""){
        $this -> idProveedor = $idProveedor;        
        $this -> estado = $estado;
        parent::__construct( $nombre , $apellido ,$docId, $correo , $clave , $foto,$pais,$departamento,$ciudad,$direccion,$telefono);
        
    }
    
    public function existeCorreo(){
        return "select correo
                from Proveedor
                where correo = '" . $this -> correo .  "'";
    }
        
    public function registrar($codigoActivacion){
        return "insert into Proveedor (correo, clave, estado)
                values ('" . $this -> correo . "', '" . md5($this -> clave) . "', '-1')";
    }
 
    
    
    public function activar(){
        return "update Proveedor 
                set estado = '1'
                where correo = '" . $this -> correo .  "'";
    }
    
    public function autenticar(){
        return "select idProveedor, estado
                from Proveedor
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto
                from Proveedor
                where idProveedor = '" . $this -> idProveedor .  "'";
    }
    
    public function consultarTodos(){
        return "select idProveedor, nombre, apellido, correo, estado
                from Proveedor";
    }
    
    public function cambiarEstado(){
        return "update Proveedor
                set estado = '" . $this -> estado . "'
                where idProveedor = '" . $this -> idProveedor .  "'";
    }
    
}

?>